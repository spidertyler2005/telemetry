package com.winterwatergames.Telemetry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.winterwatergames.Telemetry.GUI.elements.*;

public class Main {
	public String getIP(String host) {
		 try{  
			 return "ip of " + host + " is " + (java.net.InetAddress.getByName(host).getHostAddress());  
		 }catch(Exception ex){return "ERROR";}  
	}
	public static void main(String[] args) {
		Frame f = new Frame("Ip From Domain",0,0,300,300);
		Button b = new Button("Get-Ip",0,50,300,30);
		TextField text = new TextField(0,20,300,20);
		Label l = new Label("No ip",0,0,300,20);
		f.add(b.getElement());
		f.add(text.getElement());
		f.add(l.getElement());
		b.addAction(new ActionListener(){  
			@Override
			public void actionPerformed(ActionEvent e) {
				l.setText(new Main().getIP(text.getText())); 
				
			}  
		});
		f.setVisibility(true);
		
	}

}
