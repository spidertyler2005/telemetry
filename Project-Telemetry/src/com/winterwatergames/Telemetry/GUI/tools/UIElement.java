package com.winterwatergames.Telemetry.GUI.tools;

import java.awt.*;

public interface UIElement {
	
	public int[] getBounds();
	
	public Component getElement();
	
	public String getText(); //may not be supported in all UIElements
	
	public void setText(String text); //may not be supported in all UIElements
	
	public void setBounds(int x, int y, int w, int h);
}
