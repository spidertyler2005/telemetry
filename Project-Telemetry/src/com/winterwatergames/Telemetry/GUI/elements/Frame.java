package com.winterwatergames.Telemetry.GUI.elements;

import java.awt.Component;

import javax.swing.*;

public class Frame {
	private JFrame frame;
	private int w,h,x,y;
	private String name;
	
	//constructor
	public Frame(String name, int x, int y, int w, int h) {
		this.w = w;
		this.h = h;
		this.x = x;
		this.y = y;
		this.name = name;
		frame = new JFrame(name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(x, y, w, h);
		frame.setLayout(null);
	}
	
	//functions
	
	public void add(Component component) {
		frame.add(component);
	}
	
	public void addFromArray(Component[] elements) {
		for(Component x : elements) {
			frame.add(x);
		}
	}
	//getters and setters
	public JFrame getFrame() {
		return frame;
	}
	
	public int[] getBounds(){
		return new int[] {x,y,w,h};
	}
	public String getText() {
		return name;
	}
	
	public void setVisibility(boolean vis) {
		frame.setVisible(vis);
	}
	
}
