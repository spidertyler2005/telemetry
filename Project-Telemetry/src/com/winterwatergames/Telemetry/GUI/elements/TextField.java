package com.winterwatergames.Telemetry.GUI.elements;

import java.awt.Component;

import javax.swing.*;

import com.winterwatergames.Telemetry.GUI.tools.UIElement;
public class TextField implements UIElement {
	private JTextField textField;  
	private int x,y,w,h;
	
	//constructor
	public TextField(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		textField = new JTextField();
		textField.setBounds(x, y, w, h);
		
	}
	//functions
	
	public void setText(String text) {
		textField.setText(text);
	}
	
	@Override
	public String getText() {
		return textField.getText();
	}
		
	//getters and setters
	@Override
	public int[] getBounds(){
		return new int[] {x,y,w,h};
	}

	@Override
	public Component getElement() {
		return textField;
	}

	@Override
	public void setBounds(int x, int y, int w, int h) {
		this.x = x;
		this.y =y;
		this.w = w;
		this.h =h;
		textField.setBounds(x,y,w,h);
		
	}
	
}
