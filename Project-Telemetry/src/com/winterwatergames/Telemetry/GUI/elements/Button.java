package com.winterwatergames.Telemetry.GUI.elements;

import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.*;

import com.winterwatergames.Telemetry.GUI.tools.UIElement;  
public class Button implements UIElement{
	private JButton button;
	private int x, y, w, h;
	private String name = "";
	private Icon logo;
	
	public Button(String name, int x, int y, int w, int h) {
		this.x = x;
		this.y =y;
		this.w = w;
		this.h =h;
		this.name = name;
		button = new JButton(name);
		button.setBounds(x,y,w+x,h+h);
	}
	public Button(Icon logo, int x, int y, int w, int h) {
		this.x = x;
		this.y =y;
		this.w = w;
		this.h =h;
		this.logo = logo;
		button = new JButton(logo);
		button.setBounds(x,y,w,h);
	}
	
	//functions
	
	public void addAction(ActionListener action) {
		button.addActionListener(action);
	}
	
	//getters and setters
	
	@Override
	public void setBounds(int x, int y, int w, int h) {
		this.x = x;
		this.y =y;
		this.w = w;
		this.h =h;
		button.setBounds(x,y,w,h);
	}
	
	@Override
	public int[] getBounds(){
		return new int[] {x,y,w,h};
	}
	
	@Override
	public String getText() {
		return name;
	}
	
	public Icon getIcon() {
		return logo;
	}
	
	@Override
	public Component getElement() {
		return button;
	}
	
	@Override
	public void setText(String text) {
		// TODO Auto-generated method stub
		
	}
}
