package com.winterwatergames.Telemetry.GUI.elements;

import java.awt.Component;

import javax.swing.*;

import com.winterwatergames.Telemetry.GUI.tools.UIElement;

public class Label implements UIElement{
	private int x,y,w,h;
	private JLabel label;
	//constructor
	public Label(String text, int x, int y, int w, int h) {
		this.x = x;
		this.y= y;
		this.w = w;
		this.h = h;
		label = new JLabel(text);
		label.setBounds(x, y, w, h);
	}
	
	//functions
	
	@Override
	public void setText(String text) {
		label.setText(text);
	}
	
	@Override
	public String getText() {
		return label.getText();
	}
		
	//getters and setters
	
	@Override
	public int[] getBounds(){
		return new int[] {x,y,w,h};
	}

	@Override
	public Component getElement() {
		return label;
	}

	@Override
	public void setBounds(int x, int y, int w, int h) {
		this.x = x;
		this.y =y;
		this.w = w;
		this.h =h;
		label.setBounds(x,y,w,h);
		
	}
}
